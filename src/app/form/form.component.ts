import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  locationInput = '';
  valid = false;
  emailText = '';
  locationText = '';
  getValid() {
    this.valid = this.email.valid && this.locationInput !== '';
    return this.valid;
  }
  getErrorMessage() {
    return this.email.hasError('required')
      ? 'You must enter a value'
      : this.email.hasError('email')
        ? 'Not a valid email'
        : '';
  }

  submit() {
    this.emailText = 'Email: ' + this.email.value;
    this.locationText = 'Location: ' + this.locationInput;
  }

  constructor() {}

  ngOnInit() {}
}
