import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComponent } from './form.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule,
  MatOptionModule,
  MatButtonModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        MatToolbarModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule
      ],
      declarations: [FormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change email and location texts on submit', () => {
    component.email.setValue('validEmail@gmail.com');
    component.locationInput = 'Mexico';
    component.submit();
    expect(component.emailText).toBe('Email: validEmail@gmail.com');
    expect(component.locationText).toBe('Location: Mexico');
  });

  it('should catch validation errors on email', () => {
    component.locationInput = 'Canada';
    component.email.setValue('invalidEmail');
    component.getValid();
    expect(component.valid).not.toBeTruthy();
  });

  it('should allow valid emails', () => {
    component.locationInput = 'Canada';
    component.email.setValue('validEmail@gmail.com');
    component.getValid();
    expect(component.valid).toBeTruthy();
  });

  it('should catch validation errors on location', () => {
    component.email.setValue('validEmail@gmail.com');
    component.locationInput = '';
    component.getValid();
    expect(component.valid).not.toBeTruthy();
  });

  it('should allow valid locations', () => {
    component.email.setValue('validEmail@gmail.com');
    component.locationInput = 'Canada';
    component.getValid();
    expect(component.valid).toBeTruthy();
  });

  it('should display email and location text in an h3 tag', async(() => {
    component.email.setValue('validEmail@gmail.com');
    component.locationInput = 'Alaska';
    component.getValid();
    component.submit();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toContain(
      'Email: validEmail@gmail.com Location: Alaska'
    );
  }));
});
