import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private loginService: LoginService) {}
  authenticated = false;
  ngOnInit() {}

  login() {
    this.loginService.login();
  }

  logout() {
    this.loginService.logout();
  }

  authenticate() {
    return this.loginService.authenticate();
  }
}
