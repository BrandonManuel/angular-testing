import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';

import {
  MatToolbarModule,
  MatInputModule,
  MatFormFieldModule,
  MatSelectModule,
  MatOptionModule,
  MatButtonModule
} from '@angular/material';
import { LoginService } from '../login.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: LoginService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatToolbarModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule
      ],
      declarations: [LoginComponent],
      providers: [LoginService]
    }).compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(LoginService);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    service = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('authenticate should return false when not authenticated', () => {
    spyOn(service, 'authenticate').and.returnValue(false);
    expect(component.authenticate()).toBeFalsy();
    expect(service.authenticate).toHaveBeenCalled();
  });

  it('authenticate should return true when authenticated', () => {
    class MockLoginService extends LoginService {
      authenticated = true;
      authenticate() {
        console.log('calling authenticate');
        return this.authenticated;
      }
    }
    TestBed.resetTestingModule();
    TestBed.configureTestingModule({
      imports: [
        MatToolbarModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule
      ],
      declarations: [LoginComponent],
      providers: [{ provide: LoginService, useClass: MockLoginService }]
    }).compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.authenticate()).toBeTruthy();
  });
});
