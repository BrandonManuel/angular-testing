import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor() {}

  login() {
    localStorage.setItem('token', '12345');
  }

  logout() {
    localStorage.removeItem('token');
  }

  authenticate() {
    if (localStorage.getItem('token') === '12345') {
      return true;
    } else {
      return false;
    }
  }
}
