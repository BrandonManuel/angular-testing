import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  getH3Element() {
    return element(by.css('app-root h3'));
  }

  getH3Text() {
    return element(by.css('app-root h3')).getText();
  }

  getSubmitButton() {
    return element(by.buttonText('Submit'));
  }

  getEmailInput() {
    return element(by.name('email'));
  }

  getLocationSelect() {
    return element(by.name('location'));
  }

  getErrorElement() {
    return element(by.css('app-root mat-error'));
  }
}
