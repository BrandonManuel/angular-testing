import { AppPage } from './app.po';
import { Key } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to test-app!');
  });

  it('should display empty email input', () => {
    page.navigateTo();
    expect(page.getEmailInput().isPresent()).toBe(true);
    expect(page.getEmailInput().getText()).toBe('');
  });

  it('should display location select', () => {
    page.navigateTo();
    expect(page.getLocationSelect().isPresent()).toBe(true);
    expect(page.getLocationSelect().getText()).toBe('Choose a Location');
  });

  it('should initially not have the h3 tag', () => {
    page.navigateTo();
    expect(page.getH3Element().isPresent()).toBe(false);
  });

  it('should initially disable the submit button', () => {
    page.navigateTo();
    expect(page.getSubmitButton().isEnabled()).toBe(false);
  });

  it('should display error for invalid email', () => {
    page.navigateTo();
    page.getEmailInput().sendKeys('invalidEmail', Key.TAB);
    expect(page.getErrorElement().getText()).toBe('Not a valid email');
  });

  it('should not display error for valid email', () => {
    page.navigateTo();
    page.getEmailInput().sendKeys('validEmail@gmail.com', Key.TAB);
    expect(page.getErrorElement().isPresent()).toBe(false);
  });

  it('should mark location field as invalid when location is not selected', () => {
    page.navigateTo();
    page.getLocationSelect().sendKeys(Key.TAB, Key.TAB);
    expect(page.getLocationSelect().getAttribute('aria-invalid')).toBe('true');
  });

  it('should not mark location field as invalid when location is selected', () => {
    page.navigateTo();
    page.getLocationSelect().sendKeys(Key.DOWN, Key.ENTER, Key.TAB);
    expect(page.getLocationSelect().getAttribute('aria-invalid')).toBe('false');
  });

  it('should enable submit button after valid email and location', () => {
    page.navigateTo();
    page.getEmailInput().sendKeys('validEmail@gmail.com', Key.TAB);
    page.getLocationSelect().sendKeys(Key.DOWN, Key.ENTER, Key.TAB);
    expect(page.getSubmitButton().isEnabled()).toBe(true);
  });

  it('should add h3 tag with proper text after submission', () => {
    page.navigateTo();
    page.getEmailInput().sendKeys('validEmail@gmail.com', Key.TAB);
    page.getLocationSelect().sendKeys(Key.DOWN, Key.ENTER, Key.TAB);
    page.getSubmitButton().click();
    expect(page.getH3Element().isPresent()).toBe(true);
    expect(page.getH3Text()).toBe(
      'Email: validEmail@gmail.com Location: Alaska'
    );
  });
});
